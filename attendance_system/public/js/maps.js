frappe.call("frappe.client.get_value", {
	doctype: "Google Settings",
	fieldname: "api_key"
}).then(res => {
	if(!res.exc){
		let {api_key} = res.message;
		var script = document.createElement("script");
		script.src = `https://maps.googleapis.com/maps/api/js?key=${api_key}`;
		script.defer = true;
		window.initMap = function(){/* JS Api Key is leaded and avaliable */};
		console.log(script, api_key);
		document.head.appendChild(script);
	}
});
