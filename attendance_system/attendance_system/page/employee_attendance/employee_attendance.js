frappe.pages['employee-attendance'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Employee Attendance',
		single_column: true
	});

	let preview = document.getElementById("preview");
	let enroll_preview = document.getElementById("enroll_preview");
	let startButton = document.getElementById("startButton");
	let endButton = document.getElementById("endButton");
	let hourlyButton = document.getElementById("hourlyButton");
	let errorButton = document.getElementById("errorButton");
	let enrollButton = document.getElementById("enrollButton");
	let geoButton = document.getElementById("geoButton");

	wrapper.face_recognition = new frappe.FaceRecognition(wrapper, page);
}

frappe.FaceRecognition = Class.extend({
	init: function(wrapper, page) {
		var me = this;
		this.parent = wrapper;
		this.page = this.parent.page;
		setTimeout(function() {
			me.setup(wrapper, page);
		}, 0);
	},

	setup: function(wrapper, page){
		var me = this;
		this.body = $('<div></div>').appendTo(this.page.main);
		var $container = $(frappe.render_template('employee_attendance', "")).appendTo(this.body);
		me.show_profile(wrapper);
	
		get_location(page);
		geoButton.addEventListener("click", function() {
			get_location(page);
		}, false);

		startButton.addEventListener("click", function() {
			send_log('IN', 0)
		}, false);
		endButton.addEventListener("click", function() {
			send_log('OUT', 0)
		}, false);

		me.set_buttons(wrapper, page);
	},

	show_profile: function(wrapper, page){
		var me = this;

		frappe.db.get_value("Employee", {"user_id":frappe.session.user}, "*", function(r){
			let {image, employee_name, company, date_of_birth, date_of_joining, enrolled, name, status} = r;
		
			let img = `<img class='media-object' src=${image} alt='profile_picture' style='border-radius: 4px;'>`;
			const profile_html = `
				<tr><th style="text-transform: capitalize; font-size:12px; width:50%; background-color: #eee;">Employee ID</th>
                                <td><b>${name}</b></td></tr>

				<tr><th style="text-transform: capitalize; font-size:12px; width:50%; background-color: #eee;">Employee Name</th>
				<td><b>${employee_name}</b></td></tr>

				<tr><th style="text-transform: capitalize; font-size:12px; width:50%; background-color: #eee;">Company</th>
				<td><b>${company}</b></td></tr>

				<tr><th style="text-transform: capitalize; font-size:12px; width:50%; background-color: #eee;">Date Of Joining</th>
				<td><b>${date_of_joining}</b></td></tr>`;

			$("#listview").html(profile_html);
			$("#profile_image").html(img);

			//page.enrolled = r.enrolled;
			if(!r.enrolled){
				$(enrollButton).show();
			}else{
				me.check_existing(wrapper, page, r.enrolled);
			}
		});
	},

	set_buttons: function(wrapper, page){
		$('.title_message').show();
		$('#msg').empty().append(`<div class="alert alert-info" style="margin-top: 1rem;">Your Face data will appear here!!!</div>`);

		/*----enroll-----*/
		enrollButton.addEventListener("click", function() {
			$('.enrollment').show();
			$('.verification').hide();
			$('.title_message').hide();
			$('#cues').empty().append(`<div class="alert alert-danger">Please remove your spectacles. Follow the instructions here after clicking Enroll button. Starting Enrolling...</div>`);

			setTimeout(function(){
				start_enrollment();
			}, 5000);
		}, false);
	},

	check_existing(wrapper, page, enrolled){
		if(!enrolled){
			$('#endButton').show();
			$('#hourlyButton').show();
			$('#enrollButton').hide();
			$('#startButton').hide();
		}else{
			$('#endButton').hide();
			$('#hourlyButton').show();
			$('#enrollButton').hide();
			$('#startButton').show();
		}
	}
});


/*--------location start---------*/

function load_gmap(position){
	console.log("sahil", position.coords);
	let {latitude, longitude} = position.coords;
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 15,
		center: {lat: latitude, lng: longitude}
	});

	let locationMarker = new google.maps.Marker({
		map: map,
		animation: google.maps.Animation.DROP,
		position: {lat: latitude, lng: longitude}
	});

	markers.push(locationMarker);
	addYourLocationButton(map, locationMarker);
}

function get_location(page){
	if(navigator.geolocation){
		window.markers = [];
		window.circles = [];
		// JS API is loaded and available
		console.log("Called")
		navigator.geolocation.getCurrentPosition(
			position => {
				page.position = position;
				load_gmap(position);
			},
			error => {
				switch(error.code) {
					case error.PERMISSION_DENIED:
					frappe.msgprint(__(`
						<b>Please enable location permissions to proceed further.</b>
						1. <b>Firefox</b>:<br> Tools > Page Info > Permissions > Access Your Location. Select Always Ask.<br>
						2. <b>Chrome</b>:<br> Hamburger Menu > Settings > Show advanced settings.<br>In the Privacy section, click Content Settings. <br> In the resulting dialog, find the Location section and select Ask when a site tries to... .<br> Finally, click Manage Exceptions and remove the permissions you granted to the sites you are interested in.<br><br> <b>After enabling, click on the <i>Get Location</i> button</b> or <b>Reload</b>.`));

						break;
					case error.POSITION_UNAVAILABLE:
						frappe.msgprint(__("Location information is unavailable."));
						break;
					case error.TIMEOUT:
						frappe.msgprint(__("The request to get user location timed out."));
						break;
					case error.UNKNOWN_ERROR:
						frappe.msgprint(__("An unknown error occurred."));
						break;
				}
			}
		);
	} else {
		frappe.msgprint(__("Geolocation is not supported by this browser."));
	}
}

function addYourLocationButton (map, marker){
	console.log(map, marker);
	var controlDiv = document.createElement('div');
	var firstChild = document.createElement('button');
	firstChild.style.backgroundColor = '#fff';
	firstChild.style.border = 'none';
	firstChild.style.outline = 'none';
	firstChild.style.width = '40px';
	firstChild.style.height = '40px';
	firstChild.style.borderRadius = '2px';
	firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
	firstChild.style.cursor = 'pointer';
	firstChild.style.marginRight = '10px';
	firstChild.style.padding = '0';
	firstChild.title = 'Click to get your location.';
	controlDiv.appendChild(firstChild);

	var secondChild = document.createElement('div');
	secondChild.style.margin = 'auto';
	secondChild.style.width = '19px';
	secondChild.style.height = '19px';
	secondChild.style.backgroundImage = 'url(https://maps.gstatic.com/tactile/mylocation/mylocation-sprite-2x.png)';
	secondChild.style.backgroundSize = '180px 18px';
	secondChild.style.backgroundPosition = '0 0';
	secondChild.style.backgroundRepeat = 'no-repeat';
	firstChild.appendChild(secondChild);

	google.maps.event.addListener(map, 'center_changed', function () {
		secondChild.style['background-position'] = '0 0';
	});

	firstChild.addEventListener('click', function () {
		var imgX = '0',
			animationInterval = setInterval(function () {
				imgX = imgX === '-18' ? '0' : '-18';
				secondChild.style['background-position'] = imgX+'px 0';
			}, 500);

		if(navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(
				position => {
					cur_page.page.position = position;
					let latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
					map.setCenter(latlng);
					clearInterval(animationInterval);
					secondChild.style['background-position'] = '-144px 0';
				},
				error => {
					switch(error.code) {
						case error.PERMISSION_DENIED:
						frappe.msgprint(__(`
							<b>Please enable location permissions to proceed further.</b>
							1. <b>Firefox</b>:<br> Tools > Page Info > Permissions > Access Your Location. Select Always Ask.<br>
							2. <b>Chrome</b>:<br> Hamburger Menu > Settings > Show advanced settings.<br>In the Privacy section, click Content Settings. <br>In the resulting dialog, find the Location section and select Ask when a site tries to... .<br>Finally, click Manage Exceptions and remove the permissions you granted to the sites you are interested in.<br><br><b>After enabling, click on the <i>Get Location</i> button</b> or <b>Reload</b>.`));
							break;

						case error.POSITION_UNAVAILABLE:
							frappe.msgprint(__("Location information is unavailable."));
							break;
						case error.TIMEOUT:
							frappe.msgprint(__("The request to get user location timed out."));
							break;
						case error.UNKNOWN_ERROR:
							frappe.msgprint(__("An unknown error occurred."));
							break;
					}
				}
			);
		} else {
			clearInterval(animationInterval);
			secondChild.style['background-position'] = '0 0';
		}
	});
	controlDiv.index = 1;
	map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(controlDiv);
}

/*-------location end--------*/

/*-------starting_enrollment---------*/
function start_enrollment(){
	show_cues();
	navigator.mediaDevices.getUserMedia({
		video: {
			width: { ideal: 740 },
			height: { ideal: 360 },
			frameRate: {ideal: 4},//, max: 20},
			facingMode: 'user'
		},
		audio: false
	}).then((stream) => {
		window.localStream = stream;
		enroll_preview.srcObject = stream;
		enroll_preview.captureStream = enroll_preview.captureStream || enroll_preview.mozCaptureStream;
		return new Promise(resolve => enroll_preview.onplaying = resolve);
	}).then(() => {
		let recorder = new MediaRecorder(enroll_preview.captureStream());

		setTimeout(function(){ 
			$('#cover-spin').show(0);
			recorder.stop(); 
			stop(enroll_preview);
		}, 13000);
		let data = [];
		
		recorder.ondataavailable = event => data.push(event.data);
		recorder.start();

		let stopped = new Promise((resolve, reject) => {
			recorder.onstop = resolve;
			recorder.onerror = event => reject(event.name);
		});

		return Promise.all([ stopped ]).then(() => data);
	}).then ((recordedChunks) => {
		let recordedBlob = new Blob(recordedChunks, {
			type: "video/mp4",
		});
		console.log(recordedBlob);
		upload_file(recordedBlob, 'enroll');
	})	
}

function show_cues(){
	let timeleft = 15;
	let downloadTimer = setInterval(function(){
		if(timeleft <= 0){
			clearInterval(downloadTimer);
			$("#cues").empty()
		} else if(timeleft > 10) {
			$("#cues").empty().append(`<div class="alert alert-info"> <span class="cues"> ${__('Look Straight at the camera.')} <span class="countdown">${__(timeleft - 10)}</span><span></div>`);
		} else if(timeleft <= 10 && timeleft > 5) {
			$("#cues").empty().append(`<div class="alert alert-info"><i class="fa fa-arrow-left fa-icon"></i> <span class="cues"> ${__('Turn your face left slowly and return to straight position.')} <span class="countdown">${__(timeleft - 5)}</span></span></div>`);
		} else if(timeleft <=5) {
			$("#cues").empty().append(`<div class="alert alert-info"><i class="fa fa-arrow-right fa-icon"></i> <span class="cues"> ${__('Turn your face right slowly and return to straight position.')} <span class="countdown">${__(timeleft)}</span></span></div>`);
		}
		timeleft -= 1;
	}, 1000);
}

function stop(videoEl){
	localStream.getTracks().forEach( (track) => {
		track.stop();
	});
	// stop only video
	localStream.getVideoTracks()[0].stop();
	videoEl.srcObject = null;
}
/*----------enrollment_end-----------*/
// function send_logs(log_type, skip_attendance){
// 	$('.verification').hide();
// 	$('.enrollment').hide();
// 	$('.title_message').hide();
// 	$('#startButton').show();
// 	$('#endButton').hide();
// 	console.log("logig out")
// 	// upload_file("none", 'verify', log_type, skip_attendance);
// 	frappe.call({
// 		method: "attendance_system.attendance_system.page.employee_attendance.logouttt",
		
// 		callback: function (Response) {
// 		console.log(Response.message)
// 		}
			
// 	})
// }


/*-----verify------*/
function send_log(log_type, skip_attendance){
	$('.verification').show();
	$('.enrollment').hide();
	$('.title_message').hide();

	countdown();
	navigator.mediaDevices.getUserMedia({
		video: {
			width: { ideal: 640 },
			height: { ideal: 360 },
			frameRate: {ideal: 5, max: 10},
			facingMode: 'user'
		},
		audio: false
	}).then((stream) => {
		window.localStream = stream;
		preview.srcObject = stream;
		preview.captureStream = preview.captureStream || preview.mozCaptureStream;
		return new Promise(resolve => preview.onplaying = resolve);
	}).then(() => {
		let recorder = new MediaRecorder(preview.captureStream());
		setTimeout(function(){
			$('#cover-spin').show(0);
			recorder.stop();
			stop(preview);
		}, 5000);
		let data = [];

		recorder.ondataavailable = event => data.push(event.data);
		recorder.start();

		let stopped = new Promise((resolve, reject) => {
			recorder.onstop = resolve;
			recorder.onerror = event => reject(event.name);
		});
		return Promise.all([ stopped ]).then(() => data);
	}).then ((recordedChunks) => {
		let recordedBlob = new Blob(recordedChunks, {
			type: "video/mp4",
		});
		console.log(recordedBlob, skip_attendance);
		upload_file(recordedBlob, 'verify', log_type, skip_attendance);
	});
}

/*--------uploading_file---------*/
function upload_file(file, method, log_type, skip_attendance){
	let method_map = {
		'enroll': '/api/method/attendance_system.attendance_system.page.employee_attendance.employee_attendance.enroll',
		'verify': '/api/method/attendance_system.attendance_system.page.employee_attendance.employee_attendance.verify'
	}

	return new Promise((resolve, reject) => {
		let xhr = new XMLHttpRequest();
		xhr.open("POST", method_map[method], true);
		xhr.setRequestHeader("Accept", "application/json");
		xhr.setRequestHeader("X-Frappe-CSRF-Token", frappe.csrf_token);

		let form_data = new FormData();
		form_data.append("file", file, frappe.session.user+".mp4");

		if(method == 'verify'){
			// let {timestamp} = cur_page.page.page.position;
			let {latitude, longitude} = cur_page.page.page.position.coords;
			form_data.append("latitude", latitude);
			form_data.append("longitude", longitude);
			// form_data.append("timestamp", timestamp);
			form_data.append("log_type", log_type);
			form_data.append("skip_attendance", skip_attendance);
		}

		xhr.onreadystatechange = () => {
			if (xhr.readyState == XMLHttpRequest.DONE) {
				$('#cover-spin').hide();
				if (xhr.status === 200) {
					let r = null;
					try {
						r = JSON.parse(xhr.responseText);
						console.log(r);
						frappe.msgprint(__(r.message), __("Successful"));
						//window.location.reload();
						$('.enrollment').hide();
						$('.verification').hide();
						$('.title_message').show();
						if(log_type=="IN"){
						$('#startButton').hide();
						$('#endButton').show();

						}
						else{
							$('#startButton').show();
							$('#endButton').hide();




						}


					} catch (e) {
						r = xhr.responseText;
					}
				} else if (xhr.status === 403) {
					let response = JSON.parse(xhr.responseText);
					frappe.msgprint({
						title: __("Not permitted"),
						indicator: "red",
						message: response._error_message,
					});
				} else {
					let error = null;
					try {
						error = JSON.parse(xhr.responseText);
					} catch (e) {
						// pass
						}
					frappe.request.cleanup({}, error);
				}
			}
		};
		xhr.send(form_data);
	});
}

function countdown(){
	let timeleft = 5;
	let downloadTimer = setInterval(function(){
		if(timeleft <= 0){
			clearInterval(downloadTimer);
			$("#countdown").empty();
		} else {
			$("#countdown").empty().append(`<div class="alert alert-info"><span class="cues">Blink your eyes. <span class="countdown">${timeleft}</span><span></div>`);
		}
		timeleft -= 1;
	}, 1000);
}

